package TestModels;

import Model.Bookstore;
import osmo.tester.annotation.*;

public class Authentication {

    private Bookstore bookstore;

    private static final String IDENTIFICATION = "admin@admin";
    private static final String NAME = "admin";

    @BeforeTest
    public void startTest() {
        bookstore = new Bookstore();
        System.out.println("=============== TEST START ===============");
    }

    @Guard("login")
    public boolean allowLogin() {
        return (bookstore.getCurrentUser() == null);
    }

    @TestStep("login")
    public void login() {
        System.out.println("\nLOGIN");
        bookstore.login(IDENTIFICATION);
    }

    @Guard("logout")
    public boolean allowLogout() {
        return (bookstore.getCurrentUser() != null);
    }

    @TestStep("logout")
    public void logout() {
        System.out.println("LOGOUT");
        bookstore.logout();
    }

    @Guard("register")
    public boolean allowRegister() {
        return (bookstore.getCurrentUser() == null);
    }

    @TestStep("register")
    public void register () {
        System.out.println("\nREGISTER");
        bookstore.register(IDENTIFICATION, NAME);
    }

    @AfterTest
    public void endTest() {
        System.out.println("================ TEST END ================");
    }
}
