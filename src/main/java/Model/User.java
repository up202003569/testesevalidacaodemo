package Model;

import java.util.ArrayList;
import java.util.List;

public class User extends Observer {
    private String identification;
    private String name;
    private List<Book> books;
    private List<String> notifications;

    public User(String identification, String name) {
        this.identification = identification;
        this.name = name;
        books = new ArrayList<Book>();
        notifications = new ArrayList<String>();
    }

    public void addNotification(String notification) {
        notifications.add(notification);
    }

    public void clearNotification() {
        notifications.clear();
    }

    public void printNotifications() {
        System.out.println(notifications.toString());
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
