package Model;

public interface Subject {

    public void notifyAllObservers(String notification);

    public void attach(Observer observer);

    public void detach(Observer observer);
}
