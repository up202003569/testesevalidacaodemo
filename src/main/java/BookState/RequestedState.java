package BookState;

import Model.Book;

import java.util.Random;

public class RequestedState implements BookState {
    public boolean request(Book book) {
        return false;
    }

    public boolean deliver(Book book) {

        int low=0, high=10;
        Random random=new Random();
        int res=random.nextInt(((high-low)+1)-low);

        // Book has 50% to become defective
        if(res>=5) {
            /**
             * TODO
             * set book has defects to true
             * set book state to DamagedState
             */
        }
        else
            book.setState(new AvailableState());
        return true;
    }


    public boolean repair(Book book) {
        return false;
    }
}
