package Test;

import TestModels.Authentication;
import osmo.tester.OSMOTester;
import osmo.tester.generator.endcondition.Length;

public class TestAuthentication {
    public static void main(String[] args) {
        OSMOTester tester = new OSMOTester();
        tester.addModelObject(new Authentication());
        tester.setTestEndCondition(new Length(6));
        tester.setSuiteEndCondition(new Length(8));
        tester.generate(52);
    }

}
